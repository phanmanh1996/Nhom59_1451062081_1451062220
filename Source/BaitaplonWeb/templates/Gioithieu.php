<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="css/myapp.css">
	<link rel="stylesheet" type="text/css" href="css/gioithieu.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/menu.js"></script>
	<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
	<title>Document</title>
</head>
<body>
	<div class="menu">
		<div class="row">
     <div class="col-sm-3"></div>
     <div class="col-sm-9">
		<ul>
			<li><a href="myapp.html">Trang chủ</a></li>
			<li><a href="">Sản phẩm </a>
			   <ul class="sub-menu">
			   <li><a href="">Ăn chay</a></li>
			   <li><a href="">Đồ ăn đặc biệt</a></li>
			   <li><a href="">Thức uống </a></li>
			   </ul>
			</li>
			<li><a href=""> Giới thiệu </a></li>
			<li><a href=""> Cửa hàng </a></li>
			<li><a href=""> Liên hệ đặt hàng </a></li>
			<li><a href="#"><span class="glyphicon glyphicon-log-in"></span>Đăng nhập </a></li>
		</ul>
	</div>
	</div>
	</div>
	<div class="center">
	</div>
	<div class="footer">
		<div class="container">
			<div class="top-footer">
				<div class="row">
					<div class="col-md-4 con-xs-12 col-sm-12">
						<h3>Liên hệ với chúng tôi</h3>
						<ul>
							<li>
								<i class="fa fa-map-marker"></i>
								 Số 175, Tây Sơn , Đống Đa - Hà Nội 
							</li>
							<li>
								<i class="fa fa-phone"></i>
								 0971794596 
							</li>	
							<li>
								<i class="fa fa-phone"></i>
								 0944614296 
								<span style="color: #881D1A;">
								<i class="fa fa-envelope"></i>
								 Đồ ăn nhanh online 123 
								</span>
							</li>
						</ul>
					</div>
					<div class="col-md-2 col-xs-12">
					</div>
					<div class="col-md-2 col-xs-12">
						
					</div>
					<div class="col-md-2 col-xs-12">
						<ul style="padding-top: 50px;">
							<li>
								<a href="/">Trang chủ</a>
							</li>
							<li>
								<a href="">Sản phẩm</a>
							</li>
							<li>
								<a href="">Giới thiệu</a>
							</li>
							<li>
								<a href=""> Cửa hàng </a>
							</li>
						</ul>
					</div>
					<div class="col-md-2 col-xs-12">
						
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>