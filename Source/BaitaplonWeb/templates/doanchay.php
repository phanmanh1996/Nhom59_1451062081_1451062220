<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="css/myapp.css">
	<link rel="stylesheet" type="text/css" href="css/anchay.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/menu.js"></script>
	<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
	<title>Document</title>
</head>
<body>
	<div class="menu" style="height: auto;">
			<div class="row">
     <div class="col-sm-3"></div>
     <div class="col-sm-9">
		<ul>
			<li><a href="index.php">Trang chủ</a></li>
			<li><a href="index.php">Sản phẩm </a>
			</li>
			<li><a href="gioithieu.html"> Giới thiệu </a></li>
			<li><a href="cuahang.html"> Cửa hàng </a></li>
			<li><a href="lienhedathang.html"> Liên hệ đặt hàng </a></li>
			<li><a href="#"><span class="glyphicon glyphicon-log-in"></span>Đăng nhập</a></li>
		</ul>
	</div>
	</div>
	</div>
	<div class="center">
		<div class="container text-center">
	<h3><a href="anchay.html"> Các món chay  </a></h3>
	<div class="row">
	<div class="col-sm-4">
	<img src="image/banhbaochay.jpg" class="img-thumbnail" alt="Banh bao chay" width="304" height="236">
	<p> Bánh bao chay </p>
	<p> Giá : 10.000vnd / chiếc </p>
	</div>
	<div class="col-sm-4">
	<img src="image/banhngosuadua.jpg" class="img-thumbnail" alt=" bánh ngô sữa dừa " width="304" height="236">
	<p> Bánh ngô sữa dừa </p>
	<p> Giá : 15.000vnd / chiếc </p>
	</div>
	<div class="col-sm-4">
	<img src="image/banhkhoai.jpg" class="img-thumbnail" alt="Banh khoai" width="304" height="236">
	<p> Bánh khoai</p>
	<p> Giá : 10.000vnd / chiếc </p>
	</div>
	<div class="col-sm-4">
	<img src="image/banhxeo.jpg" class="img-thumbnail" alt=" Bánh xèo " width="304" height="236">
	<p> Bánh xèo </p>
	<p> Giá : 10.000vnd / chiếc </p>
	</div>
	<div class="col-sm-4">
	<img src="image/xoinhieumau.jpg" class="img-thumbnail" alt="Xôi nhiều màu " width="304" height="236">
	<p> Xôi nhiều màu (Giá : 30.000vnd / đĩa )</p>
	</div>
	<div class="col-sm-4">
	<img src="image/khoaitaychien.jpg" class="img-thumbnail" alt=" Khoai tây chiên" width="304" height="236">
     <p> Khoai tây chiên (Giá : 15.000vnd / đĩa) </p>
	</div>
	</div>
	</div>
	</div>
	<div class="footer" style="height: auto;">
		<div class="container">
			<div class="top-footer">
				<div class="row">
					<div class="col-md-4 con-xs-12 col-sm-12">
						<h3>Liên hệ với chúng tôi</h3>
						<ul>
							<li>
								<i class="fa fa-map-marker"></i>
								 Số 175, Tây Sơn , Đống Đa - Hà Nội 
							</li>
							<li>
								<i class="fa fa-phone"></i>
								 0971794596 
							</li>	
							<li>
								<i class="fa fa-phone"></i>
								 0944614296 
								<span style="color: #881D1A;">
								<i class="fa fa-envelope"></i>
								 Đồ ăn nhanh online 123 
								</span>
							</li>
						</ul>
					</div>
					<div class="col-md-2 col-xs-12">
					</div>
					<div class="col-md-2 col-xs-12">
						
					</div>
					<div class="col-md-2 col-xs-12">
						<ul style="padding-top: 50px;">
							<li>
								<a href="myapp.html">Trang chủ</a>
							</li>
							<li>
								<a href="myapp.html">Sản phẩm</a>
							</li>
							<li>
								<a href="gioithieu.html">Giới thiệu</a>
							</li>
							<li>
								<a href="cuahang.html"> Cửa hàng </a>
							</li>
						</ul>
					</div>
					<div class="col-md-2 col-xs-12">
						
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>