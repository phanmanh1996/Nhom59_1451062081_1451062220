<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" type="text/css" href="css/myapp.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/menu.js"></script>
	<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
</head>
<body>
	<div class="menu" style="height: auto;">
	<div class="row">
     <div class="col-sm-3"></div>
     <div class="col-sm-9">
		<ul>
			<li><a href="">Trang chủ</a></li>
			<li><a href="">Sản phẩm </a>
			   <ul class="sub-menu">
			   <li><a href="doanchay.php">Ăn chay</a></li>
			   <li><a href="">Đồ ăn đặc biệt </a></li>
			   <li><a href="">Thức uống </a></li>
			   </ul>
			</li>
			<li><a href="Gioithieu.html"> Giới thiệu </a></li>
			<li><a href=""> Cửa hàng </a></li>
			<li><a href="lienhedathang.html"> Liên hệ đặt hàng </a></li>
			<li><a href="LoginForm1"><span class="glyphicon glyphicon-log-in"></span>Đăng nhập</a></li>
		</ul>
	</div>
	</div>
	</div>
	<div class="center1">
		<div class="w3-content w3-section" style="width:100%">
  <img class="mySlides" src="image/banhbaochay.jpg" style="width:100% ;height: 500px;">
  <img class="mySlides" src="image/mipate.jpg" style="width:100% ; height:500px; ">
  <img class="mySlides" src="image/khoaitaychien.jpg" style="width:100% ; height:500px; ">
  <img class="mySlides" src="image/banhngosuadua.jpg" style="width:100% ; height:500px; ">
  <img class="mySlides" src="image/banhxeo.jpg" style="width:100% ; height:500px; ">
</div>

<script>
var myIndex = 0;
carousel();

function carousel() {
    var i;
    var x = document.getElementsByClassName("mySlides");
    for (i = 0; i < x.length; i++) {
       x[i].style.display = "none";  
    }
    myIndex++;
    if (myIndex > x.length) {myIndex = 1}    
    x[myIndex-1].style.display = "block";  
    setTimeout(carousel, 2000); // Change image every 2 seconds
}
</script>
	</div>
	<div class="center2" style="height: auto;">
	<div class="container">
	<div class="block-privacy">
	 <div class="row" style="margin-top: 10px;">
	 <div class="col-md-4 col-sm-4 col-xs-12">
	  <img src="image/plane.png" alt="">
	  <p> Miễn phí vận chuyển trong nội thành với hóa đơn trên 200.000 </p>
	 </div>
	 <div class="col-md-4 col-sm-4 col-xs-12">
	  <img src="image/gift.png" alt="">
	  <p> Tặng nước uống kèm theo khi mua hóa đơn trên 300.000</p>
	 </div>
	 <div class="col-md-4 col-sm-4 col-xs-12">
	  <img src="image/star.png" alt="">
	  <p> Chiết khấu 10% cho các đơn hàng trên 1.000.000 </p>
	 </div>
	 </div>
	</div>
	</div>
	<div class="container text-center">
	<h3><a href="anchay.html"> Các món chay  </a></h3>
	<div class="row">
	<div class="col-sm-4">
	<img src="image/banhbaochay.jpg" class="img-thumbnail" alt="Banh bao chay" width="304" height="236">
	<p> Bánh bao chay </p>
	<p> Giá : 10.000vnd / chiếc </p>
	</div>
	<div class="col-sm-4">
	<img src="image/banhngosuadua.jpg" class="img-thumbnail" alt=" bánh ngô sữa dừa " width="304" height="236">
	<p> Bánh ngô sữa dừa </p>
	<p> Giá : 15.000vnd / chiếc </p>
	</div>
	<div class="col-sm-4">
	<img src="image/banhkhoai.jpg" class="img-thumbnail" alt="Banh khoai" width="304" height="236">
	<p> Bánh khoai</p>
	<p> Giá : 10.000vnd / chiếc </p>
	</div>
	<div class="col-sm-4">
	<img src="image/banhxeo.jpg" class="img-thumbnail" alt=" Bánh xèo " width="304" height="236">
	<p> Bánh xèo </p>
	<p> Giá : 10.000vnd / chiếc </p>
	</div>
	<div class="col-sm-4">
	<img src="image/xoinhieumau.jpg" class="img-thumbnail" alt="Xôi nhiều màu " width="304" height="236">
	<p> Xôi nhiều màu (Giá : 30.000vnd / đĩa )</p>
	</div>
	<div class="col-sm-4">
	<img src="image/khoaitaychien.jpg" class="img-thumbnail" alt=" Khoai tây chiên" width="304" height="236">
     <p> Khoai tây chiên (Giá : 15.000vnd / đĩa) </p>
	</div>
	</div>
	</div>
	<div class="container text-center">
	<h3><a href="anchay.html"> Các món đặc biệt  </a></h3>
	<div class="row">
	<div class="col-sm-4">
	<img src="image/banhbotloc.jpg" class="img-thumbnail" alt="Bánh bột lọc " width="304" height="236">
	<p> Bánh bột lọc (Giá : 20.000vnd / đĩa)  </p>
	</div>
	<div class="col-sm-4">
	<img src="image/banhmibate.jpg" class="img-thumbnail" alt=" bánh mì ba tê " width="304" height="236">
	<p> Bánh mì ba tê (Giá : 15.000vnd / chiếc)</p>
	</div>
	<div class="col-sm-4">
	<img src="image/banhpiza.jpg" class="img-thumbnail" alt="Banh piza" width="304" height="236">
	<p> Bánh piza (Giá : 80.000vnd / chiếc) </p>
	</div>
	</div>
	</div>
	<div class="container text-center">
	<div class="row">
	<div class="col-sm-4">
	<img src="image/comduabo.jpg" class="img-thumbnail" alt=" cơm rang dưa bò " width="304" height="236">
	<p>Cơm rang dưa bò (Giá : 30.000vnd /suất)  </p>
	</div>
	<div class="col-sm-4">
	<img src="image/nemchuaran.jpg" class="img-thumbnail" alt="Nem chua rán " width="304" height="236">
	<p> Nem chua rán (Giá : 20.000vnd / đĩa)  </p>
	</div>
	<div class="col-sm-4">
	<img src="image/comga.jpg" class="img-thumbnail" alt=" cơm gà " width="304" height="236">
	<p> Cơm gà (Giá : 30.000vnd / suất )  </p>
	</div>
	</div>
	</div>
	<div class="container text-center">
	<h3><a href="anchay.html"> Đồ uống   </a></h3>
	<div class="row">
	<div class="col-sm-4">
	<img src="image/nuocmia.jpg" class="img-thumbnail" alt=" Nước mía " width="304" height="236">
	<p> Nước mía (Giá : 15.000vnd / Cốc)  </p>
	</div>
	<div class="col-sm-4">
	<img src="image/trasua.jpg" class="img-thumbnail" alt=" Trà sữa " width="304" height="236">
	<p> Trà sữa (Giá : 15.000vnd /Cốc)</p>
	</div>
	<div class="col-sm-4">
	<img src="image/sinhto.jpg" class="img-thumbnail" alt=" Sinh tố " width="304" height="236">
	<p> Sinh tố các loại (Giá :30.000vnd /Cốc) </p>
	</div>
	</div>
	</div>
	</div>
	<div class="footer" style="height: auto;">
		<div class="container">
			<div class="top-footer">
				<div class="row">
					<div class="col-md-4 con-xs-12 col-sm-12">
						<h3>Liên hệ với chúng tôi</h3>
						<ul>
							<li>
								<i class="fa fa-map-marker"></i>
								 Số 175, Tây Sơn , Đống Đa - Hà Nội 
							</li>
							<li>
								<i class="fa fa-phone"></i>
								 0971794596 
							</li>	
							<li>
								<i class="fa fa-phone"></i>
								 0944614296 
								<span style="color: #881D1A;">
								<i class="fa fa-envelope"></i>
								 Đồ ăn nhanh online 123 
								</span>
							</li>
						</ul>
					</div>
					<div class="col-md-2 col-xs-12">
					</div>
					<div class="col-md-2 col-xs-12">
						
					</div>
					<div class="col-md-2 col-xs-12">
						<ul style="padding-top: 50px;">
							<li>
								<a href="#">Trang chủ</a>
							</li>
							<li>
								<a href="#">Sản phẩm</a>
							</li>
							<li>
								<a href="gioithieu.html">Giới thiệu</a>
							</li>
							<li>
								<a href="cuahang.html"> Cửa hàng </a>
							</li>
						</ul>
					</div>
					<div class="col-md-2 col-xs-12">
						
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>